import { Title } from '@angular/platform-browser';

export interface Film {
    id?: number,
    name?: string,
    description?: string,
    imageURL?: string,
    created_at?: Date
};