import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Film } from '../interfaces/Film';

@Injectable({
  providedIn: 'root'
})
export class FilmService {

  API_URI = 'http://localhost:3000';

  constructor(private http: HttpClient) { }

  getFilms() {
    return this.http.get(`${this.API_URI}/product`);
  }

  getFilm(id: string) {
    return this.http.get(`${this.API_URI}/product/${id}`);
  }

  saveFilm(film: Film) {
    return this.http.post(`${this.API_URI}/product/create`, film);
  }
}
