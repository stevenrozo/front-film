import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders} from '@angular/common/http';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  private url = 'http://localhost:3000';

  constructor(
    private http: HttpClient,
    private router: Router
  ) { }

  loggedIn() {
    if (localStorage.getItem('nameUser')) {
      return true;
    }
    else {
      return false;
    }
  }

  loggedOut() {
    localStorage.removeItem('nameUser');
    localStorage.removeItem('usuUser');
    localStorage.removeItem('passUser');
    this.router.navigate(['/login']);
  }

  validateUserRegister(logData: any) {
    const headers = new HttpHeaders({
    'Content-Type': 'application/json',
    // tslint:disable-next-line: max-line-length
   
  });
    return this.http.get( `${this.url}/usuario/${logData.user}/${logData.pass}`,{headers});
}
}
