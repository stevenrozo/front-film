import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders} from '@angular/common/http';
import { Router } from '@angular/router';
import {DatosUser} from '../interfaces/Isignup';

@Injectable({
  providedIn: 'root'
})
export class SignupService {

  private url = 'http://localhost:3000';

  constructor(private http: HttpClient,
    private router: Router) { }

  
  saveData(DatosUser: DatosUser){
    const body =  JSON.stringify(DatosUser);
    console.log("BODY :", body);
    
    const headers = new HttpHeaders({
      'Content-Type': 'application/json'
    });

    return this.http.post<any>(this.url+'/usuario/create', body, {headers} );
  }
}
