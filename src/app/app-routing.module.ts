import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {LoginComponent} from './components/login/login.component'
import {SingUpComponent} from './components/sing-up/sing-up.component'
import {FilmListComponent} from './components/film-list/film-list.component'
import {FilmFormComponent} from './components/film-form/film-form.component'

const routes: Routes = [
  {
    path: '',
    redirectTo: 'login',
    pathMatch: 'full'
  },
  {
    path: 'login',
    component: LoginComponent ,
  },
  {
    path: 'signUp',
    component: SingUpComponent ,
  },
  {
    path: 'films',
    component: FilmListComponent ,
  },
  {
    path: 'films/add',
    component: FilmFormComponent
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
