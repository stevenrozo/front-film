import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { SignupService } from 'src/app/services/signup.service';
import Swal from 'sweetalert2';
import {DatosUser} from '../../interfaces/Isignup';

@Component({
  selector: 'app-sing-up',
  templateUrl: './sing-up.component.html',
  styleUrls: ['./sing-up.component.css']
})
export class SingUpComponent implements OnInit {
  respuesta: any = {};
  DatosUser: DatosUser = {
    name: '',
    user: '',
    pass: ''
  };

  constructor(private signupService: SignupService, private router: Router) { }

  ngOnInit(): void {
  }
  message(mensaje: string){
    Swal.fire({
      icon: 'error',
      title: 'Oops...',
      text: mensaje
    })
  }

  validate()
  
  {
    let pswCifrada = '';
    if (this.DatosUser.name.length < 1
       || this.DatosUser.user.length < 1
       || this.DatosUser.pass.length < 1
       ) {
      this.message('Todos los campos son obligatorios');
      
    }
    else {
     this.saveData();
      
    }
  }

  saveData(){
   this.signupService.saveData(this.DatosUser).subscribe(
     res => {
       this.respuesta = res
       if(this.respuesta.message = "User Successfully Created"){
        this.router.navigate(['login']);
        Swal.fire({
          icon: 'success',
          title: 'Registro guardado',
          text: 'Ingrese su usuario y contraseña para acceder a la plataforma films',
          showConfirmButton: true,
          timer: 10000
        })
       }
       else{
        this.message('No se pudo crear el usuario!');
       }
        console.log(res);
        
     },
     err => {
       this.message('No se pudo crear el usuario!');
       console.log(err);
       
     }
   )
  }

}
