import { Component, HostBinding, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Film } from 'src/app/interfaces/Film';
import { FilmService } from 'src/app/services/film.service';

@Component({
  selector: 'app-film-form',
  templateUrl: './film-form.component.html',
  styleUrls: ['./film-form.component.css']
})
export class FilmFormComponent implements OnInit {

  
  @HostBinding('class') clases = 'row';

  film: Film = {
    id: 0,
    name: '',
    description: '',
    imageURL: ''
  };

  edit: boolean = false;
  constructor(private filmService: FilmService, private router: Router, private activatedRoute: ActivatedRoute) { }

  ngOnInit(): void {
    const params = this.activatedRoute.snapshot.params;
    if (params.id) {
      this.filmService.getFilm(params.id)
        .subscribe(
          res => {
            console.log(res);
            this.film = res;
            this.edit = true;
          },
          err => console.log(err)
        )
    }
  }

  saveNewFilm() {
    delete this.film.created_at;
    delete this.film.id;
    this.filmService.saveFilm(this.film)
      .subscribe(
        res => {
          console.log(res);
          this.router.navigate(['/films']);
        },
        err => console.error(err)
      )
  }

}
