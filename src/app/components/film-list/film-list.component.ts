import { Component, HostBinding, OnInit } from '@angular/core';
import { FilmService } from 'src/app/services/film.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-film-list',
  templateUrl: './film-list.component.html',
  styleUrls: ['./film-list.component.css']
})
export class FilmListComponent implements OnInit {

  @HostBinding('class') classes = 'row';
  films: any = [];

  constructor(private filmService: FilmService ) { }

  ngOnInit(): void {
    this.getFilms();
  }

  getFilms() {
    this.filmService.getFilms()
      .subscribe(
        res => {
          this.films = res;
        },
        err => console.error(err)
      );
  }

  calificacion(val: any){
    for(let i=0; i<5; i++){
      if(i<val){
        var el = <HTMLVideoElement>document.getElementById((i+1)+'e')
        el.style.color = 'orange';
      }else{
        var el = <HTMLVideoElement>document.getElementById((i+1)+'e')
        el.style.color = 'black';
      }
    }
    Swal.fire({
      title: '¡Súper!',
      text: "¿Desea calificar ésta película con un puntaje de "+ val + " estrellas?",
      icon: 'info',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Sí, calificar'
    }).then((result) => {
      if (result.isConfirmed) {
        Swal.fire(
          '¡Calificada con éxito!',
          'Gracias por preferirnos',
          'success'
        )
      }
    })
  }

}
