import { Component, OnInit } from '@angular/core';
import { User } from '../../interfaces/user' ;
import Swal from 'sweetalert2'
import { AuthService } from 'src/app/services/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  constructor(
    private authService: AuthService,
    private router: Router
  ) { }

  dataRegister: any = {};
  userData: User = {
    user: '',
    pass: ''
  };

  ngOnInit(): void {
  }

  message(mensaje: string){
    Swal.fire({
      icon: 'info',
      title: 'Oops...',
      text: mensaje
    })
  }

  validate()
  {
    let pswCifrada = '';
    if (this.userData.user.length < 1 ) {
      this.message('ingrese el usuario');
      
    }
    else {
      this.validarRegistrado();
      
    }
  }

  validarRegistrado(){
    this.authService.validateUserRegister(this.userData).subscribe(
      res => {
        this.dataRegister = res;
        if(this.dataRegister != ""){
          this.router.navigate(['films']);
        }else{
          this.message("El usuario no se encuentra registrado");
        }
      },
      err => {
        console.error(err);
      }
    )

    
  }

}
